package com.first.wandchat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import com.first.wandchat.R;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class Profile extends Activity {

	private String phoneNumber;
	private String Name;
	private Button nextPage, camera, gallery, nextA;
	ImageView imagecapture;
	private ProgressDialog pd;
	private Object data;
	private int resultcode;
	Bitmap bit;
	int degits = 10;
	String name;
	String image_url;
	String phone_numbers[];
	String verified_numbers[];
	String classurl, url;
	private Intent intent;
	ProgressDialog progress;
	static String object_id;
	String str_image, sharedObjId, sharedCountry, sharedPhoneNo;
	SQLiteDatabase db;
	ArrayList<String> contact_wothout_countrycode = new ArrayList<String>();

	HashMap<String, String> contact_list;
	private static final int CAMERA_REQUEST = 11;
	private static final int YOUR_SELECT_PICTURE_REQUEST_CODE = 232;
	public static String MYPREFEERENCES = "MyPrefs";
	SharedPreferences sharedPreferences;
	ArrayList<String> contactnumber = new ArrayList<String>();
	ArrayList<String> contactname = new ArrayList<String>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.prifile);
		nextPage = (Button) findViewById(R.id.btnnext);

		imagecapture = (ImageView) findViewById(R.id.imagecapture);
		sharedPreferences = getSharedPreferences(MYPREFEERENCES,
				Profile.this.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPreferences.edit();
		sharedObjId = sharedPreferences.getString("objectId", "");
		sharedCountry = sharedPreferences.getString("country", "");
		sharedPhoneNo = sharedPreferences.getString("phone_no", "");
		Toast.makeText(Profile.this, sharedPhoneNo, Toast.LENGTH_LONG).show();

		object_id = sharedObjId;

		Log.d("object", object_id);
		classurl = Constants.url_file;
		url = classurl + object_id + ".png";

		imagecapture.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {

				final Dialog dialog = new Dialog(Profile.this);
				dialog.setContentView(R.layout.profile_image_dialoge);

				dialog.show();
				camera = (Button) dialog.findViewById(R.id.camera);
				gallery = (Button) dialog.findViewById(R.id.gallery);

				camera.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						dialog.cancel();
						Intent caintent = new Intent(
								android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
						startActivityForResult(caintent, CAMERA_REQUEST);
					}

				});

				gallery.setOnClickListener(new OnClickListener() {

					public void onClick(View v) {
						dialog.cancel();
						Intent photoPickerIntent = new Intent(
								Intent.ACTION_GET_CONTENT);
						photoPickerIntent.setType("image/*");
						startActivityForResult(photoPickerIntent, 1);

					}
				});

			}
		});

		nextPage.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {

				ContentResolver contentresolver = getContentResolver();

				// A cursor is required to store the results from the database
				// query
				Cursor cursor = contentresolver.query(
						ContactsContract.Contacts.CONTENT_URI, null, null,
						null, null);
				int count = cursor.getCount();
				if (count > 0) {

					while (cursor.moveToNext()) {

						String columnId = ContactsContract.Contacts._ID;
						int cursorIndex = cursor.getColumnIndex(columnId);

						String id = cursor.getString(cursorIndex);

						String name = cursor.getString(cursor
								.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

						int numCount = Integer.parseInt(cursor.getString(cursor
								.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)));

						if (numCount > 0) {
							Cursor phoneCursor = contentresolver
									.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
											null,
											ContactsContract.CommonDataKinds.Phone.CONTACT_ID
													+ " = ?",
											new String[] { id }, null);

							while (phoneCursor.moveToNext()) {
								String phoneNo = phoneCursor.getString(phoneCursor
										.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
								
								// Log.d("score", phoneNo);
								contactnumber.add(phoneNo);
								// Log.d("score", contactnumber.get(0));

							}

							phoneCursor.close();
						}
					}
				}

				contact_wothout_countrycode = return_list(contactnumber);

				Log.d("score", "array without countrycode -- "
						+ contact_wothout_countrycode);

				phone_numbers = new String[contact_wothout_countrycode.size()];

				for (int j = 0; j < contact_wothout_countrycode.size(); j++) {

					phone_numbers[j] = contact_wothout_countrycode.get(j);

				}

				ParseObject verify = new ParseObject("verification");

				ParseQuery<ParseObject> query = ParseQuery
						.getQuery("verification");
				Log.d("score", "hi");
				query.whereContainedIn("uphoneno", Arrays.asList(phone_numbers));
				Log.d("score", "buy");
				progress = new ProgressDialog(Profile.this);
				progress.setTitle("Please Wait...");

				progress.setCancelable(true);

				progress.show();
				query.findInBackground(new FindCallback<ParseObject>() {

					public void done(List<ParseObject> contactList,
							ParseException e) {

						if (e == null) {

							verified_numbers = new String[contactList.size()];
							for (int k = 0; k < contactList.size(); k++) {

								verified_numbers[k] = contactList.get(k)
										.getString("uphoneno");
								Log.d("score", verified_numbers[k]);

							}

							intent = new Intent(Profile.this, ContactList.class);
							Bundle b = new Bundle();
							b.putStringArray("contact", verified_numbers);
							intent.putExtra("contact_array", b);
							progress.dismiss();

							startActivity(intent);

							Log.d("score", "arr - " + verified_numbers);

						} else {
							Log.d("score", "Error: " + e.getMessage());
						}

					}

				});

			}
		});

	}

	public void onActivityResult(int requestCode, int resultcode, Intent data) {
		super.onActivityResult(requestCode, resultcode, data);
		if (requestCode == CAMERA_REQUEST) {
			
			if (data!=null) {
				
			
			Bitmap photo = (Bitmap) data.getExtras().get("data");
			photo = Bitmap.createScaledBitmap(photo, 150, 150, false);
			imagecapture.setImageBitmap(photo);

			bit = photo;
			new Upload_photo().execute(url);
		}
		}
		else if (resultcode == Activity.RESULT_CANCELED) {
			// User cancelled the image capture
		}

		else if (requestCode == 1) {
			if (data != null && resultcode == Activity.RESULT_OK) {

				Uri selectedImage = data.getData();

				String[] filePathColumn = { MediaStore.Images.Media.DATA };
				Cursor cursor = this.getContentResolver().query(selectedImage,
						filePathColumn, null, null, null);
				cursor.moveToFirst();
				int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
				String filePath = cursor.getString(columnIndex);
				cursor.close();
				if (bit != null && !bit.isRecycled()) {
					bit = null;
				}
				bit = BitmapFactory.decodeFile(filePath);
				Bitmap bt = Bitmap.createScaledBitmap(bit, 150, 150, false);
				
				imagecapture.setImageBitmap(bt);
				new Upload_photo().execute(url);
			} else {
				Log.d("Status:", "Photopicker canceled");
			}
		}

	}

	private class Upload_photo extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... params) {
			
			Parsepart2 p2 = new Parsepart2();
			String file_detail = p2.uploadProfilePhoto(url, bit);
			System.out.println("cccccccccccccccccccccc" + file_detail);
			String associateUrl = Constants.url_verif + "/" + Profile.object_id;
			try {
				JSONObject j1 = new JSONObject(file_detail);
				name = j1.getString("name");
				Log.d("json", name);
				image_url = j1.getString("url");
				Log.d("json", image_url);

				JSONObject associate = new JSONObject();
				JSONObject imagedetail = new JSONObject();

				imagedetail.put("name", name);
				imagedetail.put("_type", "File");
				
				Log.d("json", imagedetail.toString());
				associate.put("profile_image", imagedetail);
				str_image = associate.toString();

			} catch (JSONException e) {
				e.printStackTrace();
			}
			
			String imageassocciated = p2.AssocisateProfilePhoto(associateUrl,
					str_image);

			return "a";
		}

	}

	ArrayList<String> return_list(ArrayList<String> abc) {
		ArrayList<String> abc1 = new ArrayList<String>();

		for (String i : abc) {
			
         i = getFormattedNumberString(i);
         int country_digits = i.length() - degits;
         
         
         if(country_digits>0){
        	 
        	 i = i.substring(country_digits);

				abc1.add(i);
        	 
         }else{

				abc1.add(i);

			}
			
			

			
		}

		return abc1;

	}
	
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
	finish();
	}
	
	public String getFormattedNumberString(String phnNumber)
	{
	
    	StringBuffer strBuff = new StringBuffer();
    	char c;

    	for (int i = 0; i < phnNumber.length() ; i++) {
    	    c = phnNumber.charAt(i);

    	    if (Character.isDigit(c)) {
    	        strBuff.append(c);
    	    }
    	}
	
	//Toast.makeText(this, ""+strBuff, Toast.LENGTH_SHORT).show();
	return strBuff.toString();
	}
    
}
