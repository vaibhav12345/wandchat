package com.first.wandchat;

import com.first.wandchat.R;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

public class Splash extends Activity {
	SharedPreferences sharedPref;
	static boolean isSplashDisplayed;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.splash_screen);

		sharedPref = getSharedPreferences("prefs", 0);

		SharedPreferences.Editor cheak_edit = sharedPref.edit();
		cheak_edit.putBoolean("firstRun", true);
		cheak_edit.commit(); // Very important to save the preference
		if (isSplashDisplayed == false) {
			Thread t = new Thread() {
				public void run() {
					try {
						int timer = 0;

						while (timer < 3000) {

							sleep(100);
							timer = timer + 100;
						}

						Intent intent = new Intent(Splash.this, Agreement.class);
						startActivity(intent);

					}

					catch (InterruptedException e) {

					} finally {
						finish();
					}
				}

			};

			t.start();
			isSplashDisplayed = true;

		} else {
			finish();
			Intent intent = new Intent(Splash.this, Profile.class);
			startActivity(intent);

		}

	}

}
