package com.first.wandchat;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class NetworkAccessor {

	private static NetworkAccessor networkaccessor = new NetworkAccessor();

	private NetworkAccessor() {
		Log.d("test", "Constructor of singleton class");
	}

	public static NetworkAccessor getInstance() {
		return networkaccessor;

	}

	protected boolean isNetworkAvailable(Context context) {
		ConnectivityManager connectivity = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);

		if (connectivity != null) {

			NetworkInfo[] info = connectivity.getAllNetworkInfo();
			if (info != null) {
				for (int i = 0; i < info.length; i++) {
					if (info[i].getState() == NetworkInfo.State.CONNECTED) {
						return true;

					}
				}
			}
		}
		return false;
	}
}
