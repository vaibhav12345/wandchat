package com.first.wandchat;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.util.Log;

public class DataAccessor extends SQLiteOpenHelper {

	private static DataAccessor dataAccessor;
	private static Context context;

	private static final String TABLE_MESSAGE = "message";
	private static final String TABLE_CONTACT = "contact";
	private SQLiteDatabase sqliteDatabase;
	private static final String COLUMN_NAME = "contactname";
	private static final String COLUMN_NUMBER = "contactnumber";
	private static final String COLUMN_ID = "uid";
	private static final String COLUMN_MESSAGE = "message";
	private static final String COLUMN_SENDER_ID = "sender_id";
	private static final String COLUMN_RECEIVER_ID = "receiver_id";
	private static final String DATABASE_NAME = "wandchat.db";
	private static final int DATABASE_VERSION = 1;
	private static final String DATABASE_CREATE = "create table "
			+ TABLE_MESSAGE + "(" + COLUMN_ID
			+ " integer primary key autoincrement, " + COLUMN_MESSAGE
			+ " text not null," + COLUMN_SENDER_ID + " text not null,"
			+ COLUMN_RECEIVER_ID + " text not null);";
	private static final String fetctQueary="select * from message where (sender_id=? and receiver_id=? or sender_id=? and receiver_id=?)";
private static String selection[] = new String[4];
	private static final String DATABASE_QURARY = "create table "
			+ TABLE_CONTACT + "(" + COLUMN_ID
			+ " integer primary key autoincrement, " + COLUMN_NAME
			+ " text not null," + COLUMN_NUMBER + " text not null);";

	public DataAccessor(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);

		DataAccessor.context = context;
	}

	public static void fetchContext(Context context) {

		DataAccessor.context = context;

	}

	// method to return already existing dataAccessor object.
	public static DataAccessor getDataAccessor() {

		if (dataAccessor == null) {
			dataAccessor = new DataAccessor(context);
		}

		return dataAccessor;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(DATABASE_CREATE);
		db.execSQL(DATABASE_QURARY);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS" + TABLE_MESSAGE);
		onCreate(db);

	}

	public void insertMessage(String message, String senderId, String receiverId) {

		sqliteDatabase = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(COLUMN_MESSAGE, message);

		values.put(COLUMN_SENDER_ID, senderId);
		values.put(COLUMN_RECEIVER_ID, receiverId);

		sqliteDatabase.insert(TABLE_MESSAGE, null, values);
		sqliteDatabase.close();

	}

	public Cursor fetchConversation( String senderId, String receiverId) {

		sqliteDatabase = this.getWritableDatabase();
		selection[0]=senderId;
		selection[1]=receiverId;
		selection[2]=receiverId;
		selection[3]=senderId;
		
		 Log.d("score", "array element one-"+selection[0]);
		 Log.d("score", "array element two-"+selection[1]);
		 Log.d("score", "array element three-"+selection[2]);
		 Log.d("score", "array element four-"+selection[3]);
		 
	Cursor cursorQuery = sqliteDatabase.rawQuery(fetctQueary ,selection);
	       return cursorQuery;
		

	}

}
