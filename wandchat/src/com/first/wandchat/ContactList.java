package com.first.wandchat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.first.wandchat.R;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class ContactList extends Activity {

	private ListView contact_list;
	private String KEY_CONTACT = "contact";
	private String KEY_CONTACT_ARRAY = "contact_array";
	private String KEY_SHARED_PHONE = "phone_no";
	private String KEY_SHARED_SYNC="isFirstLaunch";
	private String KEY_INTENT_SENDER_PHONE = "senderPhone";
	private String KEY_INTENT_RECEIVER_PHONE = "receiverPhone";
	private String str_shared_phoneno, receiver_phoneno;
	
	public static String MYPREFEERENCES = "MyPrefs";
	int degits=10;
	
	
	 String isFirstRun;
	private String listItem;
	String[] contacts;

	private Intent message_intent;
	private SharedPreferences sharedPreferences;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.contactlist);
		sharedPreferences = getSharedPreferences(MYPREFEERENCES,
				ContactList.MODE_PRIVATE);
		//SharedPreferences.Editor editor = sharedPreferences.edit();
		str_shared_phoneno = sharedPreferences.getString("phone_no", "");
		
		
		contact_list = (ListView) findViewById(R.id.contactlist);
		Intent in = getIntent();
		Bundle b = in.getBundleExtra(KEY_CONTACT_ARRAY);
		 contacts = b.getStringArray(KEY_CONTACT);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, android.R.id.text1,
				contacts);

		contact_list.setAdapter(adapter);
		contact_list.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {

				receiver_phoneno = contact_list.getItemAtPosition(position)
						.toString();

				message_intent = new Intent(ContactList.this,
						DetailMessage.class);
				message_intent.putExtra(KEY_INTENT_SENDER_PHONE,
						str_shared_phoneno);

				message_intent.putExtra(KEY_INTENT_RECEIVER_PHONE,
						receiver_phoneno);
				startActivity(message_intent);
			}

		});

	}
	
	
	

}
