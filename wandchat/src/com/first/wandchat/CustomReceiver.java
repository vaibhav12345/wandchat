package com.first.wandchat;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Toast;

public class CustomReceiver extends BroadcastReceiver {
	private static final String TAG = "CustomReceiver";
	private String KEY_MESSAGE = "message";

	private String KEY_SENDER_ID = "senderId";
	private String KEY_RECEIVER_ID = "receiverId";

	@Override
	public void onReceive(Context context, Intent intent) {

		try {
			DetailMessage detail_msg; // = new DetailMessage();
			
			detail_msg = DetailMessage.detailMessage;
			
			String action = intent.getAction();
			String channel = intent.getExtras().getString("com.parse.Channel");
			JSONObject json;
			json = new JSONObject(intent.getExtras()
					.getString("com.parse.Data"));
			String message = json.getString(KEY_MESSAGE);
			String senderId = json.getString(KEY_SENDER_ID);
			String receiverId = json.getString(KEY_RECEIVER_ID);

			DataAccessor dataAccessor = DataAccessor.getDataAccessor();
			dataAccessor.insertMessage(message, senderId, receiverId);
			if (DetailMessage.isActivityRunning == true) {
				if (DetailMessage.receiver_phone_no.equals(senderId)) {

					detail_msg.getMessageFromReceiver(message);

				}
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

	
	

}
